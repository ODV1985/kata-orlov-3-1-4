package ru.kata.spring.boot_security.demo.dataloader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import ru.kata.spring.boot_security.demo.models.Role;
import ru.kata.spring.boot_security.demo.models.User;
import ru.kata.spring.boot_security.demo.repositories.UserRepository;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@Component
public class DataLoader implements ApplicationRunner {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(12);

    @Autowired
    public DataLoader(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void run(ApplicationArguments args) {
        User user = new User();
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(new Role(1L, "ADMIN"));

        user.setId(1L);
        user.setEmail("root@root");
        user.setPassword("root");
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setName("Dima");
        user.setSurname("Orlov");
        user.setAge(36);
        user.setRoles(roleSet);
        userRepository.save(user);

        user.setId(2L);
        user.setEmail("olya@olya");
        user.setPassword("olya");
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setName("Olya");
        user.setSurname("Mokina");
        user.setAge(34);
        user.setRoles(roleSet);
        userRepository.save(user);

        roleSet.clear();
        roleSet.add(new Role(2L, "USER"));

    }
}
